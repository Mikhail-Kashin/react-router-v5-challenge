import React from "react";
import { getUsers } from "./services/user-data/getUsers";
import { getUserPosts } from "./services/user-data/getUserPosts";

function App() {
	console.log("users", getUsers());
	console.log("posts", getUserPosts(2));
	return <div className="App">hello</div>;
}

export default App;

import { Users } from "../../types/types";

export async function getUsers() {
	try {
		const usersResponse = await fetch(`https://jsonplaceholder.typicode.com/users`);
		const usersData = await usersResponse.json();
		// const users = usersData.result;
		return usersData as Users[];
	} catch (err) {
		console.log(err);
	}
}

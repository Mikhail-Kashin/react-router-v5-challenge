import { UserPosts } from "../../types/types";

export async function getUserPosts(id: number) {
	try {
		const usersPostsResponse = await fetch(`https://jsonplaceholder.typicode.com/posts?userId=${id}`);
		const usersPostsData = await usersPostsResponse.json();
		return usersPostsData as UserPosts[];
	} catch (err) {
		console.log(err);
	}
}

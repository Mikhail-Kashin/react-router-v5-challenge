# Routing with a SPA part 1

## Learning objectives

- learn to setup routing within a SPA React application
- learn to link and navigate using history
- learn to navigate to a details page from a list
- learn routing flow and about private routes (think user auth)
- learn to redirect unauthorized users

## Description of exercise

For this exercise, we will be using CRA (create-react-app) and setting up a single page application. Previously, in NX you may have noticed that there was an option to do this for you and this is true but I want you to understand the tooling and better understand the flow. You will also gain insight into user authentication flow which will be mocked with localStorage. Plan out ahead of time how the state should track a user as it will be one of the more important parts.

## Acceptance Criteria

- A user will signup
- A user will have different nav links and greeting when "logged in"
- User cannot access pages unless they are logged in
- A logged in user can see a list of users and click on each user to see more details about them
- A logged in user can see details about the user and also the posts that they have made.

## Technical Requirements

- The user auth mocked by a fetch and storing the user and token in localStorage
  - the user Object should have properties to track if logged
  - article on localStorage vs cookies [localStorage or cookies?](https://indepth.dev/posts/1382/localstorage-vs-cookies)
- Nav will have links to users list and home when logged in
- Nav will have link to singup when logged out
- if user isn't logged in they will redirected to signup page
- all routes should take you to appropriate locations
- useEffect(s) should triggered appropriately
- Appropriate use of CSS via Material UI
- Behavior tests for inputs and components that have user interactions
- No auth libraries may be used.
- Must use npm i react-router-dom@5.3.0
- use preselected APIs (fakeStoreAPIs & JasonPlaceholder)

https://fakestoreapi.com/docs
https://jsonplaceholder.typicode.com/users
